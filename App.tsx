import { StatusBar } from 'expo-status-bar';
import React, {useContext, useState, useEffect} from 'react';
import { StyleSheet, Text, View, ImageBackground } from 'react-native';
import MainCard from './comp/MainCard';
import BlueCard from './comp/BlueCard';

export const ThemeContext = React.createContext()

export default function App() {

async function apiCall() {
    const response = await fetch('https://oppm2mcloud.openpathproducts.com/api/solar/monthly')
      .then(response => response.json())
      .then(data => {
        console.log(data);
        setData(data);
      })
      
}

  const [dataState, setData] = useState([])



useEffect(() => {
  apiCall
}, [])

const image = require('./assets/2.jpg');

  return (
    <View style={styles.container}>
      <ImageBackground source={image} style={styles.image}>
        <ThemeContext.Provider value={dataState}>
          <MainCard />
        </ThemeContext.Provider>
      </ImageBackground>
      <BlueCard />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    minHeight: '85%',
    //Centers white card and makes image background responsive
    resizeMode: 'cover',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

