import React from 'react';
import { render } from 'react-dom';
import {StyleSheet, View } from 'react-native';
import { Card } from 'react-native-elements';

//Renders blue card on the bottom of the screen
const BlueCard = () => {
    return(
        <Card containerStyle={styles.blueCardStyle}/>
    );
}

const styles = StyleSheet.create({
    blueCardStyle: {
        width: '100%',
        height: '15%',
        backgroundColor: '#3498db',
        borderColor: 'none',
        margin: '0%'
    }
})

export default BlueCard;