import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { VictoryPie, VictoryTooltip, VictoryLabel, VictoryChart, VictoryScatter, VictoryTheme, VictoryBar, VictoryLine } from './Victory/Victory';

const FirstChart = () => {

const data = [
  {quarter: 1, earnings: 13000},
  {quarter: 2, earnings: 16500},
  {quarter: 3, earnings: 14250},
  {quarter: 4, earnings: 19000}
];


  return (
    <VictoryChart
  theme={VictoryTheme.material}
>
  <VictoryLine
    interpolation="natural"
    style={{
      data: { stroke: "#c43a31" },
      parent: { border: "1px solid #ccc"}
    }}
    data={[
      { x: 1.5, y: 2 },
      { x: 2, y: 3 },
      { x: 3.5, y: 5.5 },
      { x: 4, y: 4 },
      { x: 5, y: 7 }
    ]}
    animate={{
  duration: 2000,
  onLoad: { duration: 1000 }
}}
  />
</VictoryChart>


  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default FirstChart
