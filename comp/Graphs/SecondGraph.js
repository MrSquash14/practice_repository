import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
  } from "react-native-chart-kit";
import React from "react";
import { StyleSheet, View, ImageBackground, Text, Image, Dimensions } from 'react-native';


const Chart2 = (props) => {

    return(
        <View style={{flex:1, justifyContent:'center', alignItems: 'center', display: props.num == 2 ? 'block' : 'none'}}>
            <Text>{props.num}12 </Text>
  <LineChart
    data={{
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [
        {
          data: [
             100,
            200,
            300,
            300,
            500,
           600,
            100,
          ]
        }
      ]
    }}
    width={Dimensions.get("window").width / 2} // from react-native
    height={Dimensions.get("window").height / 2}
    yAxisLabel="$"
    yAxisSuffix="K"
    yAxisInterval={1} // optional, defaults to 1
    chartConfig={{
      backgroundColor: "white",
      backgroundGradientFrom: "white",
      backgroundGradientTo: "white",
      decimalPlaces: 2, // optional, defaults to 2dp
      color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
      labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
      style: {
        borderRadius: 16
      },
      propsForDots: {
        r: "5",
        strokeWidth: ".1",
        stroke: "#ffa726"
      }
    }}
    bezier
    style={{
      marginVertical: 20,
      borderRadius: 16,
      padding: 20,
      justifyContent: "center",
      alignItems: "center"
    }}
  />
</View>
    )
}

export default Chart2

