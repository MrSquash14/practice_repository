import React, {useState, useEffect} from 'react';
import { StyleSheet, View, ImageBackground, Text, Image, Dimensions } from 'react-native';
import { Card } from 'react-native-elements'
import FirstChart from './Graphs/FirstGraph.js'
import Chart2 from './Graphs/SecondGraph.js'
import Chart3 from './Graphs/ThirdGraph.js'
import Chart4 from './Graphs/FourthGraph.js'

//MainCard function renders white card on screen.
const MainCard = () => {
  const [num, setNum] = useState(1);

  if (num == 2) {
    setNum(1)
  }

  useEffect(() => {
    const interval = setInterval(() => {
      setNum(prevNum => prevNum + 1);
    }, 6000);
    return () => clearInterval(interval);
  }, []);
 



    return(
        <Card containerStyle={styles.cardStyle} wrapperStyle={{flex: 1}}>
            <FirstChart num={num} />
            <Chart2 num={num} />
            <Chart3 num={num} />
            <Chart4 num={num} />
        </Card>
    );
}

const styles = StyleSheet.create({
    cardStyle: {
      height: '90%',
      width: '80%',
      margin: 0,
      padding: 0
    }
  });

export default MainCard;

